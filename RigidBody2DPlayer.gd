extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const SPEED = 200

func _init():
	position = PlayerVariables.position
	set_linear_velocity(PlayerVariables.motion)
	PlayerVariables.from_blackhole_world = true



# Called every frame. 'delta' is the elapsed time since the previous frame.
# warning-ignore:unused_argument
func _physics_process(delta):
	PlayerVariables.motion = get_linear_velocity()
	PlayerVariables.position = position  # for level transition to blackhole gravity
