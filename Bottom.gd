extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String, FILE, "*.tscn") var fall_down_world

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


# warning-ignore:unused_argument
func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			PlayerVariables.motion = Vector2(0,0)
			PlayerVariables.position = Vector2(411,338)
# warning-ignore:return_value_discarded
			get_tree().change_scene(fall_down_world)
