anyone can contribute and extend "bun - the game"

all you need to build is the open-source game engine godot
which you can download from their website  
https://godotengine.org/download/linux

(on the initial godot screen you can click import and navigate to this project folder)

(you can click the play button or go to project>export and choose your platform)