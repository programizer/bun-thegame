extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 20
const SPEED = 200
const JUMP_HEIGHT = -800
var motion = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	if PlayerVariables.from_blackhole_world:
		position = PlayerVariables.position
	else:
		position.y = PlayerVariables.position.y
		
	PlayerVariables.from_blackhole_world = false
	
	motion = PlayerVariables.motion

# warning-ignore:unused_argument
func _physics_process(delta):
	
	var acc = Input.get_accelerometer()

	motion.y += GRAVITY
	
	if ( Input.is_action_pressed("ui_right") or (acc.x > 2) ):
		motion.x = SPEED
	elif ( Input.is_action_pressed("ui_left") or (acc.x < -2) ):
		motion.x = -SPEED
	else:
		motion.x = 0
	
	if is_on_floor():
		if ( Input.is_action_just_pressed("ui_up") or (acc.y < -11) ):
			motion.y = JUMP_HEIGHT
	
	motion = move_and_slide(motion, UP)
	
	PlayerVariables.motion = motion  # global variable
	PlayerVariables.position = position  # for level transitions
